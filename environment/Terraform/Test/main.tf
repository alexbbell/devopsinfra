terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.token
  folder_id = var.folder_id
  zone      = var.zone

}


#Create network
resource "yandex_lb_target_group" "lb_target" {
  name      = "my-target-group"
  region_id = "ru-central1"
  description = "Target-группа для dev окружения"

  target {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    address   = "${yandex_compute_instance.vm-1.network_interface.0.ip_address}"
  }

  # target {
  #   subnet_id = "${yandex_vpc_subnet.my-subnet.id}"
  #   address   = "${yandex_compute_instance.my-instance-2.network_interface.0.ip_address}"
  # }

   depends_on = [
    yandex_compute_instance.vm-1, yandex_vpc_subnet.subnet-1
  ]
}

output "yandex_lb_target_group" { 
  value = yandex_lb_target_group.lb_target.id
}




# Внешний IP-адрес для балансировщика нагрузки
resource "yandex_vpc_address" "external-address" {
  description = "Внешний IP адрес для балансировщика нагрузки dev окружения"
  name = "${var.env}-external-address"
  external_ipv4_address {
    zone_id = var.zone
  }
}


resource "yandex_lb_network_load_balancer" "balancer" {
  name = "my-network-load-balancer"

  listener {
    name = "my-listener"
    port = 8080
    external_address_spec {
      ip_version = "ipv4"
      address  = yandex_vpc_address.external-address.external_ipv4_address[0].address
    }
  }


  attached_target_group {
    target_group_id = yandex_lb_target_group.lb_target.id

    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = "/ping"
      }
    }
  }
}

output "yandex_lb_network_load_balancer_balancer" {
  value = yandex_vpc_address.external-address.external_ipv4_address[0].address
}