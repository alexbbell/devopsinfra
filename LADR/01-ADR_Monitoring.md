# ADR: Monitoring system for the Project "VeryCoolApp" #

 

## Context ##

We are creating a new Application service and we want to know that the Application and it’s infrastructure works and also we want to know about the preventive tasks for the continuous work of our environment. Our project is very young and that’s why we want to have a free or a low cost solution.

 

## Decision ##

After the research of the existing solutions for the monitoring, we had a short list of the available systems. They are Sematext Monitoring, SolarWinds Server, Nagios, Zabbix and Prometheus. Only Zabbix and Prometheus are open-source solutions and we can use them for free.

Prometheus has more flexible querying system than Zabbix and it requires low resources for a stable work. Also we could find a lot of communities for Prometheus and extensions (nodeexporter, alertmanager, Grafana) to give Prometheus a great functionality.

The mix of Prometheus and Grafana can be set up with Ansible scripts that we use for the provisioning of our environment. It allows us to set up and to execute the monitoring system in several minutes.

 

## Status ##

Accepted

 

## Consequences ##

The usage of monitoring allows us to get the information about the utilization of our environment and to manage resources dynamically. As a result, we will have a better service for less money.

 