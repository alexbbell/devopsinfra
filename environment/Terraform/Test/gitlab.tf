resource "yandex_compute_instance" "vm-2" {
  name = "beliaev-gitlabrun"

  resources {
    cores  = 2
    memory = 2
    core_fraction = "5"
  }

  boot_disk {
    initialize_params {
      image_id = "fd8jekrp7jglcetucr2a"
      size = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  scheduling_policy {
    preemptible = "true"
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("metadata.txt")}"
  }
}



output "internal_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.ip_address
}
output "external_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
}

