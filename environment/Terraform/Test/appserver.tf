resource "yandex_compute_instance" "vm-1" {
  name = "${var.env}-beliaev-app"

  resources {
    cores  = 2
    memory = 2
    core_fraction = "5"
  }

  boot_disk {
    initialize_params {
      image_id = "fd8jekrp7jglcetucr2a"
      size = 5
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  scheduling_policy {
    preemptible = "true"
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("metadata.txt")}"
  }
}


resource "yandex_vpc_network" "vpc_network" {
  name = "${var.env}-${var.vpc_network}"
    # Зависит от источника данных, представляющего собой ID облака в котором размещается
  #depends_on = [yandex_resourcemanager_folder.folder]
}

output "yandex_vpc_network_vpc_network_id" {
  value = yandex_vpc_network.vpc_network.id
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "${var.env}-subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc_network.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "yandex_network_id" {
  value = yandex_vpc_subnet.subnet-1.network_id
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

