

variable "zone" {
    default = "ru-central1-a"  
}

variable "folder_id" {
  default = "b1g0c6age4vl0jasfpkj"
}
#yc iam create-token
variable "token" {
  description = "token to connect"
  default = "t1.9euelZrHk8jGlJaWz5OPmJTNlpyVnu3rnpWaj5CeicbGypCbk4rNz8rHxsrl8_c7GUpf-e8NWlRT_t3z93tHR1_57w1aVFP-.KUgnAD95ZYbuRiWvMQg3vNanLm5vh9gcSe6e08ZK7Otoh4tdZ5_jq3LOOhcxdEpZfI4FRthvUlE9fIWw7s0iBw"
}


variable "env" {
  default = "prod"
}

variable "vpc_network" {
  default = "network1"
}
