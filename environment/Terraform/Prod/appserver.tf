resource "yandex_compute_instance" "vm-1" {
  name = "${var.env}-beliaev-app"

  resources {
    cores  = 2
    memory = 2
    core_fraction = "5"
  }

  boot_disk {
    initialize_params {
      image_id = "fd8jekrp7jglcetucr2a"
      size = 5
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  scheduling_policy {
    preemptible = "true"
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("metadata.txt")}"
  }
}


