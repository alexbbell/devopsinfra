terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.token
  folder_id = var.folder_id
  zone      = var.zone

}


#Create network
resource "yandex_lb_target_group" "lb_target" {
  name      = "${var.env}-target-group"
  region_id = "ru-central1"
  description = "Target-группа для ${var.env} окружения"

  target {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    address   = "${yandex_compute_instance.vm-1.network_interface.0.ip_address}"
  }

  # target {
  #   subnet_id = "${yandex_vpc_subnet.my-subnet.id}"
  #   address   = "${yandex_compute_instance.my-instance-2.network_interface.0.ip_address}"
  # }

   depends_on = [
    yandex_compute_instance.vm-1, yandex_vpc_subnet.subnet-1
  ]
}

output "yandex_lb_target_group" { 
  value = yandex_lb_target_group.lb_target.id
}




# Внешний IP-адрес для балансировщика нагрузки

resource "yandex_vpc_address" "external-address" {
  description = "Внешний IP адрес для балансировщика нагрузки ${var.env} окружения"
  #folder_id = data.yandex_resourcemanager_folder.folder.id
  name = "${var.env}-external-address"
  external_ipv4_address {
    zone_id = var.zone
  }
  # Зависит от источников данных на которые ссылается
  #depends_on = [.yandex_resourcemanager_folder.folder]
}


resource "yandex_vpc_network" "vpc_network" {
  name = "${var.env}-${var.vpc_network}"
    # Зависит от источника данных, представляющего собой ID облака в котором размещается
  #depends_on = [yandex_resourcemanager_folder.folder]
}

output "yandex_vpc_network_vpc_network_id" {
  value = yandex_vpc_network.vpc_network.id
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "${var.env}-subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc_network.id
  v4_cidr_blocks = ["192.168.20.0/24"]
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "yandex_network_id" {
  value = yandex_vpc_subnet.subnet-1.network_id
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}




resource "yandex_lb_network_load_balancer" "balancer" {
  name = "${var.env}-network-load-balancer"

  listener {
    name = "${var.env}-listener"
    port = 8080
    external_address_spec {
      ip_version = "ipv4"
      address  = yandex_vpc_address.external-address.external_ipv4_address[0].address
    }
  }


  attached_target_group {
    target_group_id = yandex_lb_target_group.lb_target.id

    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = "/ping"
      }
    }
  }
}

output "yandex_lb_network_load_balancer_balancer" {
  value = yandex_vpc_address.external-address.external_ipv4_address[0].address
}