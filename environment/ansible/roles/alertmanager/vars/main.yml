---
# vars file for roles/alertmanager

# The Alertmanager release to be installed
alertmanager_release_tag: "latest"

# The URL from which download the Alertmanager release
alertmanager_release_url: "https://github.com/prometheus/alertmanager/releases/download/v0.25.0/alertmanager-0.25.0.linux-amd64.tar.gz"


# Alertmanager user and group
alertmanager_user: "alertmanager"
alertmanager_group: "alertmanager"

# Directory containing the downloaded Alertmanager release
alertmanager_install_path: "/opt"

# Directory to which symlink the installed Alertmanager binaries
alertmanager_bin_path: "/usr/local/bin"

# Alertmanager configuration file and directory
alertmanager_config_path: "/etc/alertmanager"
alertmanager_config_file: "alertmanager.yml"

# Alertmanager configuration
# (see https://alertmanager.io/docs/alerting/configuration/)
alertmanager_config: {}

# Alertmanager templates directory and files
alertmanager_templates_path: "{{ alertmanager_config_path }}/templates"
alertmanager_templates: {}
# alertmanager_templates:
#   "example": "{{ define "slack.myorg.text" }}https://internal.myorg.net/wiki/alerts/{{ .GroupLabels.app }}/{{ .GroupLabels.alertname }}{{ end}}"

# Alertmanager WebServer listen address
alertmanager_listen_address: "127.0.0.1:9093"

# Alertmanager local storage path
alertmanager_storage_path: "/var/lib/alertmanager"
alertmanager_storage_retention: "120h"

# Alertmanager log level
alertmanager_log_level: "info"

# Additional command-line arguments to be added to the Alertmanager unit
alertmanager_additional_cli_args: ""


_alertmanager_config_default:

  global:
    # The time after which an alert is declared resolved
    # if it has not been updated.
    resolve_timeout: 5m

  # The directory from which notification templates are read.
  templates:
    - "{{ alertmanager_templates_path }}/*.tmpl"

  route:
    # Send the alerts to the defaul-receiver
    receiver: "default-receiver"

    # How long to initially wait to send a notification for a group
    # of alerts. Allows to wait for an inhibiting alert to arrive or collect
    # more initial alerts for the same group. (Usually ~0s to few minutes.)
    group_wait: 10s

    # How long to wait before sending a notification about new alerts that
    # are added to a group of alerts for which an initial notification has
    # already been sent. (Usually ~5m or more.
    group_interval: 10s

    # How long to wait before sending a notification again if it has already
    # been sent successfully for an alert. (Usually ~3h or more).
    repeat_interval: 1h

    # The labels by which incoming alerts are grouped together. For example,
    # multiple alerts coming in for server=A and alertname=LatencyHigh would
    # be batched into a single group.
    group_by: ['alertname', 'service']

  receivers:

    - name: "default-receiver"
      webhook_configs:
        - url: "http://127.0.0.1:5001/"

  # Inhibition rules allow to mute a set of alerts given that another alert is
  # firing.
  # We use this to mute any warning-level notifications if the same alert is
  # already critical.
  inhibit_rules:
    - source_match:
        severity: 'critical'
      target_match:
        severity: 'warning'
      # Apply inhibition if the alertname is the same.
      equal: ['alertname', 'service']
