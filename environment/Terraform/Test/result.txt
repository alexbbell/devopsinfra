[0m[1myandex_vpc_network.vpc_network: Refreshing state... [id=enpi3egh2n0q21jk214o][0m
[0m[1myandex_vpc_address.external-address: Refreshing state... [id=e9bvlpep4msen6b385a5][0m
[0m[1myandex_vpc_subnet.subnet-1: Refreshing state... [id=e9bl6e6jvol21kag3ls1][0m
[0m[1myandex_compute_instance.vm-1: Refreshing state... [id=fhmsg9b9b2beitl4g1fl][0m
[0m[1myandex_lb_target_group.lb_target: Refreshing state... [id=enp2mf2q784o5q9jm0j2][0m
[0m[1myandex_lb_network_load_balancer.balancer: Refreshing state... [id=enptlgkl74vgoojoo553][0m

Terraform used the selected providers to generate the following execution
plan. Resource actions are indicated with the following symbols:
  [33m~[0m update in-place
[0m
Terraform will perform the following actions:

[1m  # yandex_lb_network_load_balancer.balancer[0m will be updated in-place[0m[0m
[0m  [33m~[0m[0m resource "yandex_lb_network_load_balancer" "balancer" {
        [1m[0mid[0m[0m         = "enptlgkl74vgoojoo553"
        [1m[0mname[0m[0m       = "my-network-load-balancer"
      [31m-[0m [0m[1m[0mregion_id[0m[0m  = "ru-central1" [90m->[0m [0m[90mnull[0m[0m
        [90m# (4 unchanged attributes hidden)[0m[0m

        [90m# (2 unchanged blocks hidden)[0m[0m
    }

[0m[1mPlan:[0m 0 to add, 1 to change, 0 to destroy.
[0m[0m[1m
Do you want to perform these actions?[0m
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  [1mEnter a value:[0m [0m

Interrupt received.
Please wait for Terraform to exit or data loss may occur.
Gracefully shutting down...

