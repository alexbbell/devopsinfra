#!/bin/bash

cd 12-balancedocker/Terraform

$isok=$(terraform validate)
echo $isok
if [[ $isok==*"The configuration is valid"* ]]; then
    $yatoken=$(yc iam create-token)
    #terraform  "$yatoken"
    
    terraform apply
elif [[ $isok != *"The configuration is valid"* ]]; then
    echo $isok
fi

cd ../ansible
ansible-galaxy role ./roles/dockersrv
